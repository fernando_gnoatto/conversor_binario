# Conversor De Base Decimal para Binário
O programa funciona de forma simples, basicamente ele pede um número de base decimal para o usuário, na entrada de dados, após isso, faz o processo de conversão de base, de decimal, para binário. 

Com a utilização de uma variável auxiliar, que recebe o número lido, faz a repetição dos processos matemáticos, enquanto o  mesmo for diferente de zero. 
A Implementação de uma variável "i" faz o processo de captura do resto de divisão do número por 2. Após esse procedimento, a variável "binario" recebe ele mesmo, (incialmente declarada como 0) + "i", ou seja, o resto da divisão * "j" (que inicialmente foi declarado, como 1 (Qualquer coisa *1 = qualquer coisa). 
Após esse procedimento, o auxiliar é divido por 2, assim como o j é multiplicado por 10.
Ao fim, é printado na tela, o valor binário do número decimal. 