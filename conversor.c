#include <stdio.h>

int main(void){ 

    int decimal, aux,i,j,binario;
    char opc;

    do{
        j=1; 
        binario = 0; 

    printf("\nDigite Um Numero De Base Decimal Que Deseja Converter Para Binario:\n");
    scanf("%d",&decimal);

    aux = decimal; 

    while(aux!=0){

        i=aux%2;
        binario = binario+(i*j);
        aux = aux/2; 
        j = j*10;
    }

    printf("\nO Numero %d Convertido Para Binario Eh: %d\n",decimal,binario);

    printf("\nVoce Deseja Repetir A Aplicacao? (S/s)Para Sim, Qualquer Outra Tecla, Fecha O Aplicativo\n");
    setbuf(stdin,NULL);
    scanf("%c",&opc);

    }while(opc=='s' || opc=='S');

    return 0; 
}